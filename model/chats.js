Chats= new Mongo.Collection("chats");

Chats.allow({
  insert: function (userId, profile) {
    return userId && profile.owner === userId;
  },
  update: function (userId, profile, fields, modifier) {
//    if (userId !== profile.owner && Meteor.user().roles.indexOf('admin') < 0)
//      return false;
    return true;
  },
  remove: function (userId, profile) {
    if (userId !== profile.owner || Meteor.user().roles.indexOf('user-admin') < 0)
      return false;

    return true;
  }
});
