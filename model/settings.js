Settings = new Mongo.Collection("settings");

Settings.allow({
  insert: function (userId, profile) {
//	  console.log(userId && profile.owner === userId);
    return userId && profile.owner === userId;
  },
  update: function (userId, profile, fields, modifier) {
//    if (userId !== profile.owner && Meteor.user().roles.indexOf('admin') < 0)
//      return false;
    return true;
  },
  remove: function (userId, profile) {
    if (userId !== profile.owner && 
		Meteor.user().roles.indexOf('master') < 0 &&
		Meteor.user().roles.indexOf('admin') < 0
		){
      return false;
    }

    return true;
  }
});
