angular.module("tendon").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('login', {
        url: '/login',
        template: '<login></login>'
      })
      .state('logout', {
    	resolve: {
    		logout: () => {
    			Meteor.logout(function() {
    				$state.go('login');
    			});
    		}
    	}
      })
      .state('register', {
        url: '/register',
        template: '<register></register>'
      })
      .state('resetpw', {
        url: '/resetpw',
        template: '<resetpw></resetpw>'
      })
      .state('appview', {
        url: '/app',
        template: '<app-view></app-view>',
	  	  resolve: {
	          currentUser: ($q) => {
	            if (Meteor.userId() == null) {
	              return $q.reject('AUTH_REQUIRED');
	            }
	            else {
	              return $q.resolve();
	            }
	          }
	        }
      })
//      .state('profiles', {
//        url: '/profiles',
//        template: '<profiles></profiles>',
//        resolve: {
//            currentUser: ($q) => {
//              if (Meteor.userId() == null) {
//                return $q.reject('AUTH_REQUIRED');
//              }
//              else {
//                return $q.resolve();
//              }
//            }
//          }
//      })
      .state('profileDetails', {
        url: '/profiles/:profileId',
        template: '<profile-details></profile-details>',
        resolve: {
            currentUser: ($q) => {
              if (Meteor.userId() == null) {
                return $q.reject('AUTH_REQUIRED');
              }
              else {
                return $q.resolve();
              }
            }
          }
      })
      .state('about', {
    	  url: '/about',
//    	  template: '<leaf-pane title="About"><h2>About</h2></leaf-pane>'
    	  templateUrl: 'client/about/about.ng.html'
      })
      .state("messenger", {
    	  template: '<messages-list></messages-list>',
    	  url:'/messenger/',
    	  params: {
    		  profile: null
    	  },
    	  resolve: {
              currentUser: ($q) => {
                if (Meteor.userId() == null) {
                  return $q.reject('AUTH_REQUIRED');
                }
                else {
                  return $q.resolve();
                }
              }
            }
      })
      .state('settings', {
        url: '/settings',
        template: '<settings-list></settings-list>',
        resolve: {
            currentUser: ($q) => {
              if (Meteor.userId() == null) {
                return $q.reject('AUTH_REQUIRED');
              }
              else {
                return $q.resolve();
              }
            }
          }
      })
      .state('settingDetails', {
        url: '/settings/:settingId',
        template: '<setting-details></setting-details>',
        resolve: {
            currentUser: ($q) => {
              if (Meteor.userId() == null) {
                return $q.reject('AUTH_REQUIRED');
              }
              else {
                return $q.resolve();
              }
            }
          }
      })
      .state('settingsAdd', {
        url: '/addSetting',
        template: '<settings-new></settings-new>',
        resolve: {
            currentUser: ($q) => {
              if (Meteor.userId() == null) {
                return $q.reject('AUTH_REQUIRED');
              }
              else {
                return $q.resolve();
              }
            }
          }
      });

    $urlRouterProvider.otherwise("/app");
}]);
  
angular.module("tendon").run(["$rootScope", "$state", function($rootScope, $state) {
	
//	$rootScope.$on('$stateChangeSuccess', 
//		function(event, toState, toParams, fromState, fromParams){ 
//	    var cc = document.getElementById('mainContent');
//	    cc.scrollIntoView();
//	    cc.scrollTop=0;
//	});
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === "AUTH_REQUIRED") {
      $state.go('login');
    }
    if(error === "CONVERSATION_EXPIRED") {
    	$state.go('profiles');
    }
  });
}]);
