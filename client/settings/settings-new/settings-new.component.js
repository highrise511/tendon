angular.module("tendon").directive('settingsNew', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/settings/settings-new/settings-new.ng.html',
    controllerAs: 'settingsNew',
    controller: function ($scope, $reactive, $state) {
      $reactive(this).attach($scope);
      self = this;
 
      self.newSetting = {
		name: '',
		value: '',
        type: '',
        description: ''
      };
      
      self.addNewSetting = () => {
      	var newSetting = self.newSetting; 
          newSetting.owner = Meteor.userId();
          newSetting.created = new Date();
          Settings.insert(newSetting);
          self.newSetting = {
			name: '',
			value: '',
	        type: '',
	        description: ''
	      };
          $state.go('settings');
        };
 
    }
  }
});