angular.module('tendon').directive('settingDetails', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/settings/setting-details/setting-details.ng.html',
    controllerAs: 'settingDetails',
    controller: function ($scope, $stateParams, $reactive,$state) {
    	
      $reactive(this).attach($scope);
      
      var self = this;
      
      self.subscribe('settings');
 
      self.helpers({
        setting: () => {
          return Settings.findOne({_id: $stateParams.settingId});
        }
      });
      
      self.save = () => {
        Settings.update({_id: $stateParams.settingId}, {
          $set: {
            name: self.setting.name,
            value: self.setting.value,
            type: self.setting.type,
            description: self.setting.description,
            modifiedBy: Meteor.userId(),
            updated: new Date()
          }
        }, (error) => {
          if (error) {
//            console.log('Oops, unable to update the setting...');
          }
          else {
            $state.go('settings');
          }
        });
      };
    }
  }
});