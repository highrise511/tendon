angular.module("tendon").directive('settingsList', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/settings/settings-list/settings-list.ng.html',
    controllerAs: 'settingsList',
    controller: function ($scope, $reactive, $state) {
      $reactive(this).attach($scope);
      self = this;
 
      self.subscribe('settings');
      
      self.helpers({
        settings: () => {
          return Settings.find({});
        },
        users: () => {
          return Meteor.users.find({});
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
      });
 
    }
  }
});