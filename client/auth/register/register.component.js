angular.module("tendon").directive('register', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/auth/register/register.ng.html',
    controllerAs: 'register',
    controller: function ($scope, $reactive, $state) {
      $reactive(this).attach($scope);
      self = this;
 
      self.credentials = {
		firstName: '',
		lastName: '',
        email: '',
        password: ''
      };
      self.alerts = [];
      
      self.jobs = {
	    availableOptions: [
	      {id: '1', name: 'Dr'},
	      {id: '2', name: 'Nurse'},
	      {id: '3', name: 'Pharmasist'}
	    ],
      };
 
      self.error = '';
      self.closeAlert = function(index) {
    	  	if(self.alerts.length > 0){
    	  		self.alerts.splice(index, 1);
    	  	}
    	  };
 
      self.register = () => {
    	  
        Accounts.createUser(self.credentials, (err) => {
          if (err) {
        	setTimeout(function(){ console.log("close");self.closeAlert(1); }, 5000);
            self.alerts.push({type: 'btn-danger', msg: err.message});
          }
          else {
        	  setTimeout(function(){ console.log("close");self.closeAlert(1); }, 5000);
        	  self.alerts.push({
        		  type: 'btn-success',
        		  msg: 'Account Created For: ' + 
        		  self.credentials.firstName + " " + self.credentials.lastName});
        	  var newProfile = {
       			  owner:Meteor.userId(),
       			  firstName:self.credentials.firstName,
       			  lastName:self.credentials.lastName,
       			  job:self.jobs.selectedOption
   			  };
    	      profile = Profiles.insert(newProfile);
          }
        });
      };
    }
  }
});