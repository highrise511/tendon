angular.module("tendon").directive('login', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/auth/login/login.ng.html',
    controllerAs: 'login',
    controller: function ($scope, $reactive, $state) {
      $reactive(this).attach($scope);
      
      if(Meteor.userId()) {
    	  $state.go('appview');
      }
 
      this.credentials = {
        email: '',
        password: ''
      };
 
      this.error = '';
      
      this.getLogo = function() {
      	return "http://demo.cloudimg.io/s/height/400/http://tendonapp.herokuapp.com/img/tendon_logo.svg";
//    	  return "/img/tendon_logo.svg"
      }
 
      this.login = () => {
        Meteor.loginWithPassword(this.credentials.email, this.credentials.password, (err) => {
          if (err) {
            this.error = err;
          }
          else {
            $state.go('appview');
          }
        });
      };
    }
  }
});