angular.module('tendon').filter('displayName', function () {
  return function (user) {
    if (!user)
    	return;
    if (!user.profile)
    	return;
    var username = '';
    username = user.profile.firstName + ' ' + user.profile.lastName;
    if (user.profile && username.length > 0)
    	return user.profile.firstName;
    else if (user.username)
    	return user.username;
    else if (user.emails)
    	return user.emails[0].address;
    else
    	return user;
  }
});