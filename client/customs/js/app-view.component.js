angular.module("tendon")
.directive('appView', function() {
	  return {
		    restrict: 'E',
		    templateUrl: 'client/customs/tpls/app-view.ng.html',
		    controllerAs: 'appViewCtrl',
		    controller: function ($scope, $reactive, $state, $filter, messageService) {
		    $reactive(this).attach($scope);
		    
		    var self = this;
		    
		    var pl = Profiles.find({});
	        messageService.profileList = pl;
	        var d = new Date();
        	d.setDate(d.getDate()-1);
        	var cl = Chats.find({members: {$in: [Meteor.userId()]}});
        	messageService.chats = cl;
        	var ul = Meteor.users.find({});
        	messageService.userList = ul;

	        var logout = function() {
//				console.log('called logout');
				Meteor.logout(function() {
					$state.go('appview');
				});
				
			};
			var openMe = function() {
				$state.go('profileDetails', {'profileId':messageService.myProfile._id});
			};
//			var myProfile = Profiles.find({owner: Meteor.userId()});
			self.logout = logout;
			self.openMe = openMe;
		  
	    }
	  
	  }
	});