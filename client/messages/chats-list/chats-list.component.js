angular.module("tendon").directive('chatsList', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/messages/chats-list/chats-list.ng.html',
    controllerAs: 'chatsList',
    controller: function ($scope, $reactive, $state, $filter, messageService) {
      $reactive(this).attach($scope);
      var self = this;
      
      self.newChat = {};
      
      self.hippaCompliant = function(mDate){
    	  var date1 = mDate;
    	  var date2 = new Date();
    	  var timeDiff = Math.abs(date2.getTime() - date1.getTime());
//    	  var diffDays = Math.ceil(timeDiff / (1000 * 60 * 3600 * 24));
    	  var diffDays = Math.ceil(timeDiff / (1000 * 60 * 3600 * 24));
    	  var diffHours = Math.ceil(timeDiff / (1000 * 360));
    	  return (diffHours <= 1 && mDate);
//    	  return (diffDays <= 1 && mDate);
      }
      
      var getUserById = function(userId){
          return Meteor.users.findOne(userId);
        };
      
      self.others = function(chat){
          if (!chat)
            return;
          var notMe = [];
          angular.forEach(chat.members, function(value) {
        	  if(value !== Meteor.userId()) notMe.push(value);
          })
          if (notMe.length == 0)
        	  return "nobody";
          var pl = messageService.profileList;
          var members = [];
			pl.forEach(function(r) {
				if(notMe.indexOf(r.owner) > -1) {
					members.push(r.job.name + ' ' + r.firstName + ' ' + r.lastName);
				}
			})

          return members.toString();
        };
        
       self.openChat = function(chat) {
    	   messageService.chat = chat;
    	   var notMe = [];
           angular.forEach(chat.members, function(value) {
         	  if(value !== Meteor.userId()) notMe.push(value);
           })
           if (notMe.length == 0)
         	  return "nobody";
           var pl = messageService.profileList;
           var members = [];
 			pl.forEach(function(r) {
 				if(notMe.indexOf(r.owner) > -1) {
 					members.push(r);
 				}
 			})
 			messageService.profile = members[0];
    	   $state.go('messenger', {profile: members[0]});
       }
      
      self.helpers({
        chats: () => {
        	var myList = messageService.chats;
//        	window.scrollTo(0,document.body.scrollHeight);
        	return myList;
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
//        images: () => {
//          return Images.find({});
//        }
      });

      self.openAddNewChatModal = function () {
      };
		
	  
	  var removeChat = (chatId) => {
		    if (!this.userId) {
		      throw new Meteor.Error('not-logged-in',
		        'Must be logged to create a chat.');
		    }
		 
		    check(chatId, String);
		 
		    var chat = Chats.findOne(chatId);
		 
		    if (!chat || !_.include(chat.userIds, this.userId)) {
		      throw new Meteor.Error('chat-not-exists',
		        'Chat not exists');
		    }
		 
		    Messages.remove({ chatId: chatId });
		 
		    return Chats.remove({ _id: chatId });
		  }
 
    }
  }
});