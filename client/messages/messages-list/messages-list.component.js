angular.module("tendon").directive('messagesList', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/messages/messages-list/messages-list.ng.html',
    controllerAs: 'messagesList',
    controller: function ($scope, $reactive, $state, $filter, messageService, $anchorScroll, $location) {
      $reactive(this).attach($scope);
      var self = this;
      
      self.subscribe('messages');
      
      self.newMessage = {};
      
      var profile = messageService.profile;
      var profileId;
      if(profile && profile._id) {
    	  profileId = profile._id;
    	  self.profileMeta = {
        		  job: profile.job.name,
        		  name: profile.firstName + ' ' + profile.lastName
          };
          
          self.profile = profile;
      }
      if(!profileId){
    	  $state.go("appview");
      }
      
      self.hippaCompliant = function(mDate){
    	  var date1 = mDate;
    	  var date2 = new Date();
    	  var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    	  var diffDays = Math.ceil(timeDiff / (1000 * 60 * 3600 * 24));
    	  var diffHours = Math.ceil(timeDiff / (1000 * 360));
//    	  return (diffHours <= 1 && mDate);
    	  return (diffDays <= 1 && mDate);
      }
      
      var gotoBottom = function() {
          // set the location.hash to the id of
          // the element you wish to scroll to.
          $location.hash('bottom');

          // call $anchorScroll()
          $anchorScroll();
        };
        
        self.gotoBottom = gotoBottom;

//      console.log(Messages.find({$or: [{owner: Meteor.userId()},{recipient: Meteor.userId()}]}).fetch());
      
      self.helpers({
        messages: () => {
//        	var myList = Messages.find({
//        			$or: 
////        				[{owner: Meteor.userId()},{recipient: Meteor.userId()}]
//        		[{$and: 
//        			[{owner: Meteor.userId()},{recipient: profile.owner}]
//        		},{$and: 
//        			[{owner: profile.owner},{recipient: Meteor.userId()}]
//        		}]
//			}, {sort: {created: -1}}).fetch();
        	var myList = Messages.find({chatId:messageService.chat._id}, {sort: {created: -1}}).fetch();
        	self.gotoBottom();
        	return myList;
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
//        images: () => {
//          return Images.find({});
//        }
      });
      
      self.openAddNewMessageModal = function () {
//          $mdDialog.show({
//            template: '<add-new-message-modal></add-new-message-modal>',
//            clickOutsideToClose: true
//          });
      };
		
	  self.removeMessage = (profile) => {
//		  Messages.remove({_id: profileId});
 	  } 
 
    }
  }
});