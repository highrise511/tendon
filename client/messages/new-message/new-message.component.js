angular.module('tendon').directive('addNewMessageBar', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/messages/new-message/new-message.ng.html',
    controllerAs: 'addNewMessageBar',
    controller: function ($scope, $state, $reactive, $meteor, messageService, $anchorScroll, $location) {
      $reactive(this).attach($scope);
      
      var self = this;
      
      var profile = messageService.profile;
      
      var profileId;
      
      if(profile) {
    	  profileId = messageService.profile._id;
      }

      self.profile = profile;

      self.helpers({
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        }
      });
      
      self.newMessage = {body:''};
      
      var chatId = messageService.chat._id;
      
      var addNewMessage = () => {
    	  if(self.newMessage.body && self.newMessage.body.trim().length > 0){
	    	var newMessage = self.newMessage; 
	        newMessage.owner = Meteor.userId();
	        newMessage.created = new Date();
	        newMessage.recipient = profile.owner;
	        newMessage.public = true;
//        self.newMessage.images = (self.newMessage.images || {}).map((image) => {
//            return image._id;
//          });
	        messageId = Messages.insert(newMessage);
	        var d = new Date();
	        if(!chatId){
	        	var newChat = {};
	        	newChat.owner = Meteor.userId();
	        	newChat.created = new Date();
	        	newChat.public = true;
	        	newChat.lastMessage ; messageId;
	        	newChat.members = [Meteor.userId(), profile.owner];
	        	newChat.updated = d;
	        	chatId = Chats.insert(newChat);
	        	var cl = messageService.chats;
	        	cl.forEach(function(r){
	        		if(r._id == chatId) {
	        			messageService.chat = r;
	        			$state.go('messenger');
	        		}
	        		
	        	})
	        }else {
	        	Chats.update(chatId, { $set: {lastMessage: messageId, updated:d}});
	        }
	        Messages.update(messageId, { $set: {chatId : chatId}});
	        self.newMessage = {body: ''};
	        window.scrollTo(0,document.body.scrollHeight + 50);
	        gotoBottom();
//        $mdDialog.hide();
    	  }
      };
      
      var gotoBottom = function() {
          // set the location.hash to the id of
          // the element you wish to scroll to.
          $location.hash('bottom');

          // call $anchorScroll()
          $anchorScroll();
        };
      
      self.close = () => {
//        $mdDialog.hide();
      };
      
      self.addImages = (files) => {
          if (files.length > 0) {
            let reader = new FileReader();
   
            reader.onload = (e) => {
              $scope.$apply(() => {
                self.cropImgSrc = e.target.result;
                self.myCroppedImage = '';
              });
            };
   
            reader.readAsDataURL(files[0]);
          }
          else {
            self.cropImgSrc = undefined;
          }
        };
        

        self.saveCroppedImage = () => {
          if (self.myCroppedImage !== '') {
//            Images.insert(self.myCroppedImage, (err, fileObj) => {
//              if (!self.newMessage.images) {
//                self.newMessage.images = [];
//              }
//   
//              self.newMessage.images.push(fileObj);
//              self.cropImgSrc = undefined;
//              self.myCroppedImage = '';
//            });
          }
        };
        
        self.addNewMessage = addNewMessage;
        
    }
  }
});