angular.module('tendon').factory('messageService', function () {
var factory = {
		myProfile: {},
		profile: {},
		profileList: {},
		profileOwner: {},
		chat: {},
		chats: {},
		currentUserId: null,
		userList: {}
}
        return factory;
});