angular.module('tendon').filter('displayName', function () {
  return function (user) {
    if (!user)
      return;
    if (user.profile && user.profile.firstName)
      return user.profile.firstName + user.profile.lastName;
    else if (user.username)
    	return user.username;
    else if (user.emails)
      return user.emails[0].address;
    else
      return user;
  }
});