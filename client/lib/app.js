(function () {
	angular.module('tendon',[
	 'angular-meteor',
	 'ui.router',
	 'accounts.ui'
	 ])
	Meteor.subscribe('userData');
	function onReady() {
	  angular.bootstrap(document, ['tendon']);
	}

})();