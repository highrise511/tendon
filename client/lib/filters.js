(function () {
	angular.module("tendon").filter('reverse', function() {
	  return function(items) {
		  if(items){
			  return items.slice().reverse();
		  }
	  };
	});	
})();