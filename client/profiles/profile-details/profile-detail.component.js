angular.module('tendon').directive('profileDetails', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/profiles/profile-details/profile-details.ng.html',
    controllerAs: 'profileDetail',
    controller: function ($scope, $stateParams, $reactive,$state) {
    	
      $reactive(this).attach($scope);
      
      var self = this;
      
      self.subscribe('profiles');
 
      self.helpers({
        profile: () => {
          return Profiles.findOne({_id: $stateParams.profileId});
        }
      });
      
      self.jobs = {
	    availableOptions: [
	      {id: '1', name: 'Dr'},
	      {id: '2', name: 'Nurse'},
	      {id: '3', name: 'Pharmasist'},
	      {id:'4', name: 'Admin'}
	    ],
      };
       
      self.save = () => {
        Profiles.update({_id: $stateParams.profileId}, {
          $set: {
            firstName: self.profile.firstName,
            lastName: self.profile.lastName,
            job:self.profile.job
          }
        }, (error) => {
          if (error) {
//            console.log('Oops, unable to update the profile...');
          }
          else {
            $state.go('appview');
          }
        });
      };
    }
  }
});