angular.module('tendon').directive('addNewProfileModal', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/profiles/newProfile/newProfile.ng.html',
    controllerAs: 'addNewProfileModal',
    controller: function ($scope, $stateParams, $reactive, $mdDialog) {
      $reactive(this).attach($scope);

      this.helpers({
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        }
      });

      this.newProfile = {};

      this.addNewProfile = () => {
        this.newProfile.owner = Meteor.userId();
//        this.newProfile.images = (this.newProfile.images || {}).map((image) => {
//            return image._id;
//          });
        Profiles.insert(this.newProfile);
        this.newProfile = {};
        $mdDialog.hide();
      };

      this.close = () => {
        $mdDialog.hide();
      };
      
      this.addImages = (files) => {
          if (files.length > 0) {
            let reader = new FileReader();
   
            reader.onload = (e) => {
              $scope.$apply(() => {
                this.cropImgSrc = e.target.result;
                this.myCroppedImage = '';
              });
            };
   
            reader.readAsDataURL(files[0]);
          }
          else {
            this.cropImgSrc = undefined;
          }
        };
        

        this.saveCroppedImage = () => {
          if (this.myCroppedImage !== '') {
            Images.insert(this.myCroppedImage, (err, fileObj) => {
              if (!this.newProfile.images) {
                this.newProfile.images = [];
              }
   
              this.newProfile.images.push(fileObj);
              this.cropImgSrc = undefined;
              this.myCroppedImage = '';
            });
          }
        };
        
        this.updateDescription = ($data, image) => {
            Images.update({_id: image._id}, {$set: {'metadata.description': $data}});
          };
    }
  }
});