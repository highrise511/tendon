angular.module("tendon").directive('profiles', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/profiles/profiles-list/profiles.ng.html',
    controllerAs: 'profilesList',
    controller: function ($scope, $reactive, $state, $filter, messageService) {
      $reactive(this).attach($scope);
      var self = this;
      
      self.newProfile = {};
      
      self.helpers({
        profiles: () => {
//          var pl = Profiles.find({});
          var pl = messageService.profileList;
          return pl;
        },
        users: () => {
        	var ul = messageService.userList;
          return ul;
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
      });
      
      self.openMessenger = function(profile){
    	  var cl = messageService.chats;
    	  messageService.chat = {};
    	  cl.forEach(function(r) {
				if(r.members.indexOf(profile.owner) > -1 && 
						r.members.indexOf(Meteor.userId()) > -1) {
					messageService.chat = r;
				}
			})
    	  messageService.profile = profile;
    	  $state.go('messenger', {
    		  profile: profile
    	  })
      };
      
      self.notMe = function(profile) {
    	  if(profile.owner == Meteor.userId()){
    		  messageService.myProfile = profile;
    	  }
    	  return (profile.owner !== Meteor.userId())
      }
      
//      self.openMessenger = function(p){
//      	debugger;
//      	$state.go('messenger', {
//      		profileId: p._id
//      	});
//      }
      
      self.openAddNewProfileModal = function () {
//          $mdDialog.show({
//            template: '<add-new-profile-modal></add-new-profile-modal>',
//            clickOutsideToClose: true
//          });
        };
        
      self.getUserById = (userId) => {	
        return Meteor.users.findOne(userId);
      };
      
      self.addProfile = () => {
		  this.newProfile.owner = Meteor.user()._id;
		  Profiles.insert(this.newProfile);
		  this.newProfile = {};
	  };
		
	  self.removeProfile = (profile) => {
		  Profiles.remove({_id: profile._id});
 	  } 
 
    }
  }
});